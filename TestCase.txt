Test cases
-----------

newIntersection:
	- call method with null value
	- call method with new x y co ordinates
	- call method with string value
	- call method with already existing co ordinates
	

defineRoad:
	- invoke with new intersection that is not created.
	- invoke with already existing intersections.
	- invoke with one already existing intersection and one new Intersection
	- invoke with already existing road.
	

Navigate:
	- invoke with no intersections
	- invoke with empty destination 
	- invoke with path of already existing road.
	- invoke with no connections to the intersections.
	- invoke with intersections having two or more path.


