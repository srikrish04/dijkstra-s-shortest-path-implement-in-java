import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class MapIterator {
	ArrayList<String> alreadyVisited = new ArrayList<String>();
	HashMap<String, String> resultMap = new HashMap<String, String>();
	Set<String> toBeTraversedNodes = new HashSet<String>();

	HashMap<String, String> iterateElements(int x1, int y1, int x2, int y2,
			HashMap<String, ArrayList<String>> cityMapWithDistance) {
		
		resultMap.put(x1 + "," + y1, "0=>" + x1 + "," + y1);
		toBeTraversedNodes.add(x1 + "," + y1);
		//traverse the map to all its adjacent nodes with minimum distance
		do {


			String leastElement = null;
			Integer minNodeValue = null;
			//checks for least distance among the unvisited elements
			for (String elem : toBeTraversedNodes) {
				Integer currentNodeWeight = findWeightFromStartVal(elem, x1 + "," + y1);
				
				if (minNodeValue == null || minNodeValue > currentNodeWeight) {
					minNodeValue = currentNodeWeight;
					leastElement = elem;
				}
			}
			
			alreadyVisited.add(leastElement);
			toBeTraversedNodes.remove(leastElement);
			

			// if the lease distant element is destination then exit.
			if (leastElement.equals(x2 + "," + y2)) {

				break;
			}
			// checks for each neighboring elements of least elements. traverse until no element is left unvisited
			if (cityMapWithDistance.get(leastElement) != null) {
				ArrayList<String> neighbourVals = cityMapWithDistance.get(leastElement);

				for (int i=0;i< neighbourVals.size();i++) {
					
					String adjacentVal = neighbourVals.get(i).split("=")[0];
					if (adjacentVal.equals(x1 + "," + y1)) {
						continue;
					}
					// sums each adjacent element distance from source
					Integer sum = Integer.parseInt(neighbourVals.get(i).split("=")[1]) + findWeightFromStartVal(leastElement, x1 + "," + y1);
					//checks if computed total is lesser than the already existing distance from source.
					if (resultMap.get(adjacentVal) != null) {

						if (sum < findWeightFromStartVal(adjacentVal, x1 + "," + y1)) {
							resultMap.put(adjacentVal, Integer.parseInt(neighbourVals.get(i).split("=")[1]) + "=>" + leastElement);
						}
						
					} else {
						resultMap.put(adjacentVal, Integer.parseInt(neighbourVals.get(i).split("=")[1]) + "=>" + leastElement);
					}

					if (!alreadyVisited.contains(adjacentVal)) {
						toBeTraversedNodes.add(adjacentVal);
					}

				}
			}

		} while (!toBeTraversedNodes.isEmpty()); // traverse this list until no unvisited elements found.


		return resultMap;

	}

	void iterateCityMapForShortestPath(ArrayList<String> list, String source, String destination,
			ArrayList<String> outputDisplay) {
		//traverse map to find the shortest path
		int index = getShortIndex(list, destination);
		if (list.get(index) != null) {
			outputDisplay.add(list.get(index).split("=>")[2] + "-->" + list.get(index).split("=>")[0] + "    "
					+ list.get(index).split("=>")[1]);
		}
		if (!list.get(index).split("=>")[2].equals(source)) {
			iterateCityMapForShortestPath(list, source, list.get(index).split("=>")[2], outputDisplay);
			
		} else {
			return;
		}
	}

	int getShortIndex(ArrayList<String> list, String val) {
		// returns element index with minimum distance
		int index = -1;
		int count = 0;
		int minimumVal = -1;
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).split("=>")[0].equals(val) && Integer.parseInt(list.get(i).split("=>")[1]) != 0) {
				minimumVal = Integer.parseInt(list.get(i).split("=>")[1]);
				index = i;
				count++;
			}
		}
		//return index if the val is present only once
		if (count == 1) {

			return index;
		}
		for (int i = 0; i < list.size(); i++) {
			if (val.equals(list.get(i).split("=>")[2])) {
				if (Integer.parseInt(list.get(i).split("=>")[1]) < minimumVal
						&& Integer.parseInt(list.get(i).split("=>")[1]) != 0) {
					minimumVal = Integer.parseInt(list.get(i).split("=>")[1]);
					index = i;
				}
			}
		}
		return index;
	}

	Integer findWeightFromStartVal(String val, String source) {
		// computes distance from source for each element
		if (resultMap.get(val) != null) {
			Integer weightValue = Integer.parseInt(resultMap.get(val).split("=>")[0]);
			String visitingNode = resultMap.get(val).split("=>")[1];
			
			if (!visitingNode.equals(source)) {
				weightValue = weightValue + findWeightFromStartVal(visitingNode, source);
			}

			return weightValue;
		}
		return null;
	}

	
}
