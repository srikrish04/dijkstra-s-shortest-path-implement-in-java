import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HalifaxMap {
	HashMap<String, ArrayList<String>> cityMapWithDistance = new HashMap<String, ArrayList<String>>();
	HashMap<String, String> outputMap = new HashMap<String, String>();
	ArrayList<String> outputDisplay = new ArrayList<String>();

	boolean newIntersection(int x, int y) {
		// adds new intersection to the graph, returns false if already present
		if (!cityMapWithDistance.containsKey(x + "," + y)) {
			cityMapWithDistance.put(x + "," + y, new ArrayList<String>());

			return true;
		}
		return false;
	}

	boolean defineRoad(int x1, int y1, int x2, int y2) {
		boolean success = false;
		// creates start intersection if not present in map
		if (!cityMapWithDistance.containsKey(x1 + "," + y1)) {
			cityMapWithDistance.put(x1 + "," + y1, new ArrayList<String>());
			success = true;
		} else {
			if (!cityMapWithDistance.get(x1 + "," + y1).contains(x2 + "," + y2)) {
				//hardcoding = for splitting string
				cityMapWithDistance.get(x1 + "," + y1)
						.add(String.valueOf(x2 + "," + y2 + "=" + lenghtofEdge(x1, y1, x2, y2)));
				success = true;
			}
		}
		// creates end intersection if not present in map
		if (!cityMapWithDistance.containsKey(x2 + "," + y2)) {
			cityMapWithDistance.put(x2 + "," + y2, new ArrayList<String>());
			success = true;
		} else {
			if (!cityMapWithDistance.get(x2 + "," + y2).contains(x1 + "," + y1)) {
				cityMapWithDistance.get(x2 + "," + y2)
						.add(String.valueOf(x1 + "," + y1 + "=" + lenghtofEdge(x2, y2, x1, y1)));
				success = true;
			}
		}

		return success;
	}

	void navigate(int x1, int y1, int x2, int y2) {
		MapIterator iterator = new MapIterator();
		//iterates the map to apply djikthra's and return map with shortest distance to all elements
		outputMap = iterator.iterateElements(x1, y1, x2, y2, cityMapWithDistance);
		ArrayList<String> resultSet = new ArrayList<String>();
		for (Map.Entry<String, String> entry : outputMap.entrySet()) {
			resultSet.add(entry.getKey() + "=>" + entry.getValue().toString());
		}
		//to print path in console converting map to list
		iterator.iterateCityMapForShortestPath(resultSet, x1 + "," + y1, x2 + "," + y2, outputDisplay);
		// returns No path if destination node is not linked with source
		if(outputDisplay.isEmpty()) {
			System.out.println("No Path");
		}
		System.out.println("Path         Distance");
		for (int i = outputDisplay.size() - 1; i >= 0; i--) {
			System.out.println(outputDisplay.get(i));
			if(outputDisplay.contains(x2+","+y2)) {
				break;
			}
		}
	}

	int lenghtofEdge(int x1, int y1, int x2, int y2) {
		// finds length of two intersection
		int xn = x2 - x1;
		int yn = y2 - y1;
		return (int) Math.round(Math.sqrt((xn * xn) + (yn * yn)));
	}
}
